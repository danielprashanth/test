import os

import testinfra.utils.ansible_runner

testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
    os.environ['MOLECULE_INVENTORY_FILE']).get_hosts('all')


def test_hosts_file(host):
    f = host.file('/etc/hosts')

    assert f.exists
    assert f.user == 'root'
    assert f.group == 'root'


def test_java(host):
    java = host.package("openjdk-8-jdk")
    assert java.is_installed


def test_kubectl(host):
    kubectl = host.file("/usr/local/bin/kubectl")
    assert kubectl.exists
